using Microsoft.AspNetCore.Mvc;
using ApiAuthors.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApiAuthors.Controllers;

[ApiController]
[Route("api/authors")]
public class AuthorsController: ControllerBase
{
    private readonly ApplicationDbContext context;

    public AuthorsController(ApplicationDbContext context)
    {
        this.context = context;
    }

    [HttpGet]
    public async Task<ActionResult<List<Author>>> GetAll()
    {
        return await context.Authors.Include(x => x.Books).ToListAsync();
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Author>> GetById(Int32 id)
    {
        Author author = await context.Authors.FirstOrDefaultAsync(x => x.Id == id);
        if (author == null) { return NotFound(); }
        return author;
    }

    [HttpGet("{name}")]
    public async Task<ActionResult<Author>> GetByName(String name)
    {
        Author author = await context.Authors.FirstOrDefaultAsync(x => x.Name.Contains(name));
        if (author == null) { return NotFound(); }
        return author;
    }

    [HttpPost]
    public async Task<ActionResult> Save(Author item)
    {
        context.Add(item);
        await context.SaveChangesAsync();
        return Ok();
    }

    [HttpPut("{id:int}")]
    public async Task<ActionResult> Update(Author item, Int32 id)
    {
        if (item.Id != id)
        {
            return BadRequest("El id del autor no coincide con el id de la URL");
        }
        context.Update(item);
        await context.SaveChangesAsync();
        return Ok();
    }

    [HttpDelete("{id:int}")]
    public async Task<ActionResult> Delete(Int32 id)
    {
        var exists = await context.Authors.AnyAsync(x => x.Id == id);
        if (!exists)
        {
            return NotFound();
        }
        context.Remove(new Author() { Id = id });
        await context.SaveChangesAsync();
        return Ok();
    }
}