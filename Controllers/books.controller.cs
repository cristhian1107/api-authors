using Microsoft.AspNetCore.Mvc;
using ApiAuthors.Entities;
using Microsoft.EntityFrameworkCore;

namespace ApiAuthors.Controllers;

[ApiController]
[Route("api/books")]
public class BooksController : ControllerBase
{
    private readonly ApplicationDbContext context;

    public BooksController(ApplicationDbContext context)
    {
        this.context = context;
    }

    [HttpPost]
    public async Task<ActionResult> Save(Book item)
    {
        var existsAuthor = await context.Authors.AnyAsync(x => x.Id == item.AuthorId);
        if (!existsAuthor)
        {
            return BadRequest($"No existe el autor Id: {item.AuthorId}");
        }
        context.Add(item);
        await context.SaveChangesAsync();
        return Ok();
    }

    [HttpGet("{id:int}")]
    public async Task<ActionResult<Book>> Get(int id)
    {
        return await context.Books.Include(x => x.Author).FirstOrDefaultAsync(x => x.Id == id);
    }
}