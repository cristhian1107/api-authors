using System.ComponentModel.DataAnnotations;
using ApiAuthors.Validations;

namespace ApiAuthors.Entities;

public class Author
{
    [Required(ErrorMessage = "El campo {0} es requerido")]
    public int Id { get; set; }

    [Required(ErrorMessage = "El campo {0} es requerido")]
    [StringLength(maximumLength: 5, ErrorMessage = "El campo {0} no debe tener mas de {1} caracteres")]
    [FirstLetterCapitalized]
    public string Name { get; set; }

    [Range(18, 120)]
    public int Age { get; set; }

    [CreditCard]
    public string CreditCard { get; set; }

    [Url]
    public string WebPage { get; set; }

    public List<Book> Books { get; set; }
}